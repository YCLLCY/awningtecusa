require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get pages_home_url
    assert_response :success
  end

  test "should get commercial" do
    get pages_commercial_url
    assert_response :success
  end

  test "should get residential" do
    get pages_residential_url
    assert_response :success
  end

  test "should get contactus" do
    get pages_contactus_url
    assert_response :success
  end

end
